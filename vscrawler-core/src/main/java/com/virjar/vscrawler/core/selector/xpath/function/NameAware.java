package com.virjar.vscrawler.core.selector.xpath.function;

/**
 * Created by virjar on 17/6/6.
 */
public interface NameAware {
    String getName();
}
